<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    public $timestamps = false;
    protected $table = 'code';
    protected $fillable = [
        'Id',
        'AccessId',
        'Code',
        'Date',
        'FK_User'
    ];
}