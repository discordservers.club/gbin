<?php
/**
 * Created by PhpStorm.
 * User: Cristian
 * Date: 4/17/2019
 * Time: 7:29 PM7M3u&*.FkNtYc;*
 */
return [
    'settings' => [
        'displayErrorDetails' => false,
        'addContentLengthHeader' => false,
        'renderer' => [
            'template_path' => __DIR__ . '/../view',
        ],
        'database' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'gbin',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
    ],
];