<?php

namespace App\Controllers;

use App\App;
use App\Models\Code;
use App\Models\Users;

class HomeController extends App
{
    public function index($request, $response, $args){
        return $this->view->render($response, "home/index.twig");
    }

    public function list($request, $response, $args){
        if($this->session->exists('id') == false)
            return $this->redirect('home');

        $data = $this->db->table('code')->where('FK_User', $this->session->get('id'))->get();
        return $this->view->render($response, "snippets/list.twig", ['list' => $data]);
    }

    public function viewCode($request, $response, $args){
        $code = $this->db->table('code')->where('AccessId', $args['id'])->get();
        return $this->view->render($response, 'home/view.twig', ['code' => $code]);
    }

    public function rawCode($request, $response, $args){
        $code = $this->db->table('code')->where('AccessId', $args['id'])->get();
        return $this->view->render($response, 'home/raw.twig', ['code' => $code]);
    }

    public function saveCode($request, $response, $args){
        $data = $request->getParsedBody();
        $code = $data['Code'];
        $accessCode = $this->generateAccessCode();


        $codeObj = new Code();
        $codeObj->create([
            'AccessId' => $accessCode,
            'Code' => $code,
            'FK_User' => $this->session->get('id')
        ]);

        //$this->session->get('id')

        return json_encode([
            'success' => true,
            'AccessId' => $accessCode
        ]);

    }

    private function generateAccessCode(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}