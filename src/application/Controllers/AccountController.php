<?php

namespace App\Controllers;

use App\App;
use App\Models\Code;
use App\Models\Users;

class AccountController extends App
{
    public function login($request, $response, $args)
    {
        if($this->session->exists('id') == true)
            return $this->redirect('home');
            
        return $this->view->render($response, "account/login.twig");
    }

    public function actionLogin($request, $response, $args)
    {
        $data = $request->getParsedBody();
        $email = $data['Email'];
        $password = $data['Password'];

        $user = $this->db->table('users')->where('Email', '=', $email)->first();
        if($user){
            if(password_verify($password, $user->Password)){
                $this->session->set('email', $email);
                $this->session->set('id',$user->Id);

                return $this->redirect('home');
            }
            else{
                return $this->redirect('login');
            }
        }
        else{
            return $this->redirect('login');
        }

    }

    public function register($request, $response, $args)
    {
        if($this->session->exists('id') == true)
            return $this->redirect('home');

        return $this->view->render($response, "account/register.twig");
    }

    public function actionRegister($request, $response, $args)
    {
        $data = $request->getParsedBody();
        $username = $data['Username'];
        $email = $data['Email'];
        $password = $data['Password'];
        $tokken = $this->generateTokken();

        $user = new Users();
        $user->create([
            'Username' => $username, 
            'Email' => $email, 
            'Password' => password_hash($password, PASSWORD_DEFAULT), 
            'API_Tokken' => $tokken
        ]);

        return $this->redirect('login');

    }

    public function actionLogout($request, $response, $args){
        $this->session->delete();
        return $this->redirect('login');
    }

    private function generateTokken(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 25; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}