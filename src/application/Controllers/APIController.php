<?php

namespace App\Controllers;


use App\App;
use App\Models\Code;
use App\Models\Users;

class APIController extends App
{
    public function index($request, $response, $args){
        
        if($this->session->exists('id') == false)
        return $this->redirect('home');

        $key = $this->db->table('users')->where('Id',$this->session->get('id'))->first()->API_Tokken;
        
        return $this->view->render($response, "api/index.twig", ["key" => $key ]);


    }
    
    public function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
        $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    }

    public function getForUser($request, $response, $args){
        $key = $args['key'];
        $id = $args['id'];

        $validation = $this->db->table('users')->where('API_Tokken', '=', $key)->first();
        if($validation != null){
            $data = $this->db->table('code')->where('FK_User', '=', $validation->Id)
                                            ->where('AccessId','=',$id)->get();

            $arr = array();
            foreach ($data as $value) {
                $escaped = htmlspecialchars($value->Code, ENT_QUOTES & ~ENT_COMPAT, $encoding);
                array_push($arr, [
                    "Code" => $escaped,
                    "Link" => "view\\" . $value->AccessId,
                    "Raw Link" => "raw\\" . $value->AccessId,
                    "Date" => $value->Date
                ]);
            }
        
            return json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK );
        }
        else{
            return json_encode(
                ['error' => "key is not valid"]
            );
        }
    }

    public function uploadCod($request, $response, $args){
        $key = $args['key'];
        $code = $args['code'];

        $validation = $this->db->table('users')->where('API_Tokken', '=', $key)->first();
        if($validation != null){
            $accessCode = $this->generateAccessCode();

            $codeObj = new Code();
            $codeObj->create([
                'AccessId' => $accessCode,
                'Code' => $code,
                'FK_User' => $validation->Id
            ]);

            return json_encode([
                'success' => true,
                'AccessId' => $accessCode
            ]);

            return json_encode($arr);
        }
        else{
            return json_encode(
                ['error' => "key is not valid"]
            );
        }

    }

    public function allForUser($request, $response, $args)
    {
        $key = $args['key'];

        $validation = $this->db->table('users')->where('API_Tokken', '=', $key)->first();
        if($validation != null){
            $data = $this->db->table('code')->where('FK_User', '=', $validation->Id)->get();

            $arr = array();
            foreach ($data as $value) {
                array_push($arr, [
                    "Code" => addslashes(htmlspecialchars($value->Code)),
                    "Link" => "view\\" . $value->AccessId,
                    "Raw Link" => "raw\\" . $value->AccessId,
                    "Date" => $value->Date
                ]);
            }

            return json_encode($arr);
        }
        else{
            return json_encode(
                ['error' => "key is not valid"]
            );
        }

    }

    private function generateAccessCode(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}