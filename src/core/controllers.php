<?php

/**
 * @param $c
 * @return \App\Controllers\HomeController
 */
$container['HomeController'] = function ($c) {
    return new \App\Controllers\HomeController($c);
};

/**
 * @param $c
 * @return \App\Controllers\AccountController
 */
$container['AccountController'] = function ($c) {
    return new \App\Controllers\AccountController($c);
};

/**
 * @param $c
 * @return \App\Controllers\SetupController
 */
$container['SetupController'] = function ($c) {
    return new \App\Controllers\SetupController($c);
};

/**
 * @param $c
 * @return \App\Controllers\APIController
 */
$container['APIController'] = function ($c) {
    return new \App\Controllers\APIController($c);
};