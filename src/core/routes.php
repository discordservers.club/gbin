<?php

$app->get("/", "HomeController:index")->setName("home");
$app->get("/login", "AccountController:login")->setName("login");
$app->get("/register", "AccountController:register")->setName("register");
$app->post("/save/code", "HomeController:saveCode")->setName("saveCode");
$app->get("/view/{id}", "HomeController:viewCode")->setName("viewCode");
$app->get("/raw/{id}", "HomeController:rawCode")->setName("rawCode");
$app->post('/login', "AccountController:actionLogin")->setName("loginAction");
$app->post('/register', "AccountController:actionRegister")->setName("registerAction");
$app->get('/logout',"AccountController:actionLogout")->setName("logoutAction");
$app->get('/api', 'APIController:index')->setName("indexAPI");
$app->get('/api/{key}/get/{id}', 'APIController:getForUser')->setName("idForUser");
$app->get('/api/{key}/all', 'APIController:allForUser')->setName("allForUser");
$app->post('/api/{key}/upload/{code}', 'APIController:uploadCod')->setName("uploadCodPost");
$app->get('/list', 'HomeController:list')->setName("list");


